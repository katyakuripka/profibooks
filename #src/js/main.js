$(document).ready(function() {

    $.fn.isInViewport = function() {
        let elementTop = $(this).offset().top;
        let elementBottom = elementTop + $(this).outerHeight();

        let viewportTop = $(window).scrollTop();
        let viewportBottom = viewportTop + $(window).height();

        return elementBottom > viewportTop && elementTop < viewportBottom;
    };

    $(document).keyup(function (e) {
        if (e.key === "Escape") {
            closeEverything();
        }
    });

    $('.js-close-cross').on('click', function () {
        closeEverything();
    });

    //close everything
    function closeEverything() {
        $('html').removeClass('no-scroll');
        $('.js-overlay-block').removeClass('show');
        $('.js-popup').removeClass('show');
    }

});
