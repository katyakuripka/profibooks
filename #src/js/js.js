$( document ).ready(function() {
   $('.language').click(function(){ //turn language dropdawn
   		$('.language').toggleClass('turn');
   });
   $('.your-class').slick({
    infinite: true,
  	slidesToShow: 3,
  	slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
            dots: true,
        }
      },
        {
            breakpoint: 320,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
            }
        },
    ],
  });
   $('.book-list-slider').slick({
    infinite: true,
  	slidesToShow: 4,
  	slidesToScroll: 1,
  	prevArrow: $('#buttom-left'),
  	nextArrow: $('#buttom-right'),
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
    ],
    });
   $('.book-slider').slick({
    infinite: true,
  	slidesToShow: 4,
  	slidesToScroll: 1,
  	prevArrow: $('#arrow-left'),
  	nextArrow: $('#arrow-right'),
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
    ],
  })
   $('.leather-books-slider').slick({
    infinite: true,
  	slidesToShow: 4,
  	slidesToScroll: 1,
  	prevArrow: $('#leather-books__arrow-left'),
  	nextArrow: $('#leather-books__arrow-right'),
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
        {
        breakpoint: 320,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            }
        },
    ],
  })
 //  var elem = document.querySelector('input[type="range"]');
	// var rangeValue = function(){
	//   var newValue = elem.value;
	//   var target = document.querySelector('.value');
	//   target.innerHTML = newValue;
	// }
	// elem.addEventListener("input", rangeValue);

  $('.hamburger1').on('click', function() {
    $('.all-books').slideToggle(300, function(){
      if( $(this).css('display') === "none"){
        $(this).removeAttr('style');
        }
      });
  });
  $('.advanced-search__close').on('click', function() {
    $('.advanced-search').slideUp()
  });
   $('.search-button__img').on('click', function() {
    $('.advanced-search').slideToggle()
  });
   $('#slider-price').slider({
      range : true,
      min : 700,
      max : 2500,
      connect: true,
      values : [700,2500],
      step : 1,
      start: [700, 2500],
      orientation: "horizontal",
      animate: "slow",
      slide: function(event, ui) {
      $('#slider-price__value').html(ui.values[0] + '-' + ui.values[1]);
      }
  });
   $('.overlay-button').click(function(){
      $('.overlay').show();
      $('.js-overlay').fadeIn();
      $('.js-overlay').addClass('disabled');
   });
   $('.overlay').click(function(){
      $('.overlay').hide(); 
      $('.js-overlay').fadeOut();
  });
   $('.js-close-popup').click(function(){
      $('.overlay').hide(); 
      $('.js-overlay').fadeOut();
  });
    $('.minus').click(function () {
      var $input = $(this).parent().find('input');
      var count = parseInt($input.val()) - 1;
      count = count < 1 ? 1 : count;
      $input.val(count);
      $input.change();

      var price = $(this).parents('.overlay-popup__description-price').find('.description-price__manu span').data('default-price')

      setPrice( price, count, this )

      return false;
    });
    $('.plus').click(function () {
      var $input = $(this).parent().find('input');
      $input.val(parseInt($input.val()) + 1);
      $input.change();
      var price = $(this).parents('.overlay-popup__description-price').find('.description-price__manu span').data('default-price')

      setPrice( price, parseInt($input.val()), this )

      return false;
    });

    function setPrice( price, productAmount, _this ){
      $(_this).parents('.overlay-popup__description-price').find('.description-price__manu span').text(price * productAmount);
      $(_this).parents('.overlay-popup__cart-block').find('.aside-price__full-cost span').text(price * productAmount );
    }    
    $('.js-close').click(function(){ 
      $('.overlay-popup__description, .overlay-popup__thumb').hide(); 
  });
    $('.decree-js').click(function(){
      $('.overlay').show();
      $('.overlay-popup__login-js').fadeIn();
      $('.overlay-popup__login-js').addClass('login-js-disabled');
   });
   $('.overlay').click(function(){
      $('.overlay').hide(); 
      $('.overlay-popup__login-js').fadeOut();
  });

   $('.regiser-js').click(function(){
      $('.overlay').show();
      $('.overlay-popup__checkin').fadeIn();
      $('.overlay-popup__checkin').addClass('checkin-js-disabled');
   });
   $('.overlay').click(function(){
      $('.overlay').hide(); 
      $('.overlay-popup__checkin-js').fadeOut();
  });

   $('.custom-buttom').click(function(){
      $('.overlay').show();
      $('.overlay-popup__call-back-js').fadeIn();
      $('.overlay-popup__call-back-js').addClass('call-back-disabled');
   });
   $('.overlay').click(function(){
      $('.overlay').hide(); 
      $('.overlay-popup__call-back-js').fadeOut();
  });
   $('.call-back__close').click(function(){
      $('.overlay').hide(); 
      $('.overlay-popup__call-back-js').fadeOut();
  });

   $('.popup__to-order-js').click(function(){
      $('.overlay').show();
      $('.overlay-popup__to-order-js').fadeIn();
      $('.overlay-popup__to-order-js').addClass('to-order-disabled');
   });
   $('.overlay').click(function(){
      $('.overlay').hide(); 
      $('.overlay-popup__to-order-js').fadeOut();
  });
   $('.call-back__close-js').click(function(){
      $('.overlay').hide(); 
      $('.overlay-popup__to-order-js').fadeOut();
  });
});